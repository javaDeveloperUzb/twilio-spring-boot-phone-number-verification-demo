package uz.pdp.springbootphonenumberverificationdemo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.springbootphonenumberverificationdemo.entity.TwilioVerification;

import java.util.Optional;

public interface TwilioVerificationRepository extends JpaRepository<TwilioVerification, Integer> {
    Optional<TwilioVerification> findByPhoneNumberAndVerifiedFalse(String phoneNumber); //bazada oldindan bor yoki yoqligini tekshirish
    Optional<TwilioVerification> findByPhoneNumberAndVerifiedTrue(String phoneNumber); //bazada oldindan bor yoki yoqligini tekshirish
}
